#!/bin/bash
# Choose Arch Linux x86_64 in the boot menu
# Follow these commands to setup the OS
# This commands do not include the installation of a window manager/desktop environment/ or any other additional software

timedatectl set-ntp true
timedatectl status
fdisk -l
fdisk /dev/sda  
#In fdisk, "m" for help
#In fdisk, "o" for DOS partition or "g" for GPT
g
#In fdisk, "n" for add new partition
# First Sector +550M
# Second Sector +2G
# Third Sector the rest
n
#In fdisk, "p" for primary partition (if using MBR instead of GPT)
#In fdisk, "t" to change partition type
t
1
1
t
2
19

#In fdisk, "w" (write table to disk)
w

#make filesystem:
mkfs.fat -F32 /dev/sda1
mkswap /dev/sda2
swapon /dev/sda2
mkfs.ext4 /dev/sda3

#mounts it to mnt on live image
mount /dev/sda3 /mnt
pacstrap /mnt base linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab

#Chroot:
#(change into root directory of our new installation)
arch-chroot /mnt 
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc (sets the hardware clock)
pacman -S nano
nano /etc/locale.gen
# uncomment en_US.UTF8
locale-gen
nano /etc/hostname # add archvbox

# add
# 127.0.0.1     localhost
# ::1           localhost
# 127.0.1.1     localhost.localdomain archvbox
# to the following file
nano /etc/hosts

#Users and passwords:
passwd (set root pass)
useradd -m username (make another user)
passwd username (set that users password)
usermod -aG wheel,audio,video,optical,storage username

#Sudo:
pacman -S sudo
EDITOR=nano visudo

# Uncomment the following line
%wheel ALL=(ALL) ALL

#GRUB with EFI:
pacman -S grub
pacman -S  efibootmgr dosfstools os-prober mtools (if doing UEFI)
mkdir /boot/EFI (if doing UEFI)
mount /dev/sda1 /boot/EFI  #Mount FAT32 EFI partition (if doing UEFI)
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --efi-directory=/boot/efi --no-nvram --removable #(if doing UEFI)
grub-mkconfig -o /boot/grub/grub.cfg

#Networking:
pacman -S networkmanager
systemctl enable NetworkManager

#Reboot:
#exit the chroot by typing "exit"
umount -l /mnt #(unmounts /mnt)
sudo reboot #(or shutdown now if doing this in VirtualbBox)
#Remember to detach the ISO in VirtualBox before reboot.

# install drivers
sudo pacman -S xf86-video-fbdev xorg xorg-xinit nitrogen picom vim firefox git base-devel

# Enable the AUR
git clone https://aur.archlinux.org/yay-git.git
cd yay-git
makepkg -si

# Get stuff from AUR using yay
yay alcaritty
yay nerd-fonts-mononoki

# copy and edit xinitrc
cp /etc/X11/xinit/xinitrc /home/ah/.xinitrc

# Enable services
sudo pacman -Syu sddm
systemctl enable sddm

